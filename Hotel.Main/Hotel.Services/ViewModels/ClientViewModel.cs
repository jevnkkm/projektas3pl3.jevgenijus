﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.ViewModels
{
    public class ClientViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool IsAgency { get; set; }

        public _Agency Agency { get; set; }

        public class _Agency
        {
            public string Name { get; set; }
        }
    }
}
