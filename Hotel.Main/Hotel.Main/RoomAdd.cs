﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Services;
using Hotel.ViewModels;
using Hotel.Dal.Enumerations;

namespace Hotel.Main
{
    public partial class RoomAdd : BaseForm
    {
        private RoomAdminService _roomAdminService;
        private TempRoomAddViewModel _viewModel;
        public RoomAdd(int? reservationId = null, TempRoomAddViewModel viewModel = null)
        {
            InitializeComponent();
            _roomAdminService = new RoomAdminService();
            ConfigureForm();
            if (viewModel == null)
            {
                  //Naujas kambarys
                 _viewModel = new TempRoomAddViewModel { ReservationId = reservationId };
            } else
            {
                //Redaguojamas
                FillForm(viewModel);
            }
            txtReservationId.Text = _viewModel.ReservationId.ToString();
        }

        private void ConfigureForm()
        {
            cmbxRoomType.DataSource = Enum.GetValues(typeof(RoomTypeEnum));
            cmbxRoomType.SelectedIndex = -1;
            cmbxAccoId.DataSource = Enum.GetValues(typeof(AccommodationTypeEnum));
            cmbxAccoId.SelectedIndex = -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _viewModel.RoomTypeId = (int)cmbxRoomType.SelectedItem;
            _viewModel.AccommodationTypeId = (int)cmbxAccoId.SelectedItem;
            _viewModel.Quantity = int.Parse(txtQuantity.Text);

            _roomAdminService.SaveTempRoom(_viewModel);
            this.Close();
        }

        private void FillForm(TempRoomAddViewModel viewModel)
        {
            _viewModel = viewModel;
            cmbxRoomType.SelectedItem = (RoomTypeEnum)viewModel.RoomTypeId;
            cmbxAccoId.SelectedItem = (AccommodationTypeEnum)viewModel.AccommodationTypeId;
            txtQuantity.Text = viewModel.Quantity.ToString();
            txtId.Text = viewModel.Id.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
