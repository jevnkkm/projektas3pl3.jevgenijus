﻿namespace Hotel.Main
{
    partial class Reservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpCheckIn = new System.Windows.Forms.DateTimePicker();
            this.dtpCheckOut = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewRooms = new System.Windows.Forms.DataGridView();
            this.checkBoxParking = new System.Windows.Forms.CheckBox();
            this.cmbxParkingPlaces = new System.Windows.Forms.ComboBox();
            this.lblParkingPlaces = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxIsGroup = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddRoom = new System.Windows.Forms.Button();
            this.txtReservationId = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRooms)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpCheckIn
            // 
            this.dtpCheckIn.CustomFormat = "yyyy-MM-dd";
            this.dtpCheckIn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCheckIn.Location = new System.Drawing.Point(13, 25);
            this.dtpCheckIn.Name = "dtpCheckIn";
            this.dtpCheckIn.Size = new System.Drawing.Size(93, 20);
            this.dtpCheckIn.TabIndex = 0;
            this.dtpCheckIn.Value = new System.DateTime(2018, 11, 26, 16, 53, 14, 0);
            // 
            // dtpCheckOut
            // 
            this.dtpCheckOut.CustomFormat = "yyyy-MM-dd";
            this.dtpCheckOut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCheckOut.Location = new System.Drawing.Point(112, 25);
            this.dtpCheckOut.Name = "dtpCheckOut";
            this.dtpCheckOut.Size = new System.Drawing.Size(92, 20);
            this.dtpCheckOut.TabIndex = 1;
            this.dtpCheckOut.Value = new System.DateTime(2018, 11, 26, 16, 53, 14, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Check In";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Check out";
            // 
            // dataGridViewRooms
            // 
            this.dataGridViewRooms.AllowUserToAddRows = false;
            this.dataGridViewRooms.AllowUserToDeleteRows = false;
            this.dataGridViewRooms.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRooms.Location = new System.Drawing.Point(13, 52);
            this.dataGridViewRooms.Name = "dataGridViewRooms";
            this.dataGridViewRooms.ReadOnly = true;
            this.dataGridViewRooms.Size = new System.Drawing.Size(309, 96);
            this.dataGridViewRooms.TabIndex = 4;
            this.dataGridViewRooms.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRooms_CellContentClick);
            this.dataGridViewRooms.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewRooms_DataError);
            // 
            // checkBoxParking
            // 
            this.checkBoxParking.AutoSize = true;
            this.checkBoxParking.Location = new System.Drawing.Point(7, 34);
            this.checkBoxParking.Name = "checkBoxParking";
            this.checkBoxParking.Size = new System.Drawing.Size(62, 17);
            this.checkBoxParking.TabIndex = 5;
            this.checkBoxParking.Text = "Parking";
            this.checkBoxParking.UseVisualStyleBackColor = true;
            this.checkBoxParking.CheckedChanged += new System.EventHandler(this.checkBoxParking_CheckedChanged);
            // 
            // cmbxParkingPlaces
            // 
            this.cmbxParkingPlaces.FormattingEnabled = true;
            this.cmbxParkingPlaces.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbxParkingPlaces.Location = new System.Drawing.Point(75, 32);
            this.cmbxParkingPlaces.Name = "cmbxParkingPlaces";
            this.cmbxParkingPlaces.Size = new System.Drawing.Size(53, 21);
            this.cmbxParkingPlaces.TabIndex = 6;
            this.cmbxParkingPlaces.Visible = false;
            // 
            // lblParkingPlaces
            // 
            this.lblParkingPlaces.AutoSize = true;
            this.lblParkingPlaces.Location = new System.Drawing.Point(75, 13);
            this.lblParkingPlaces.Name = "lblParkingPlaces";
            this.lblParkingPlaces.Size = new System.Drawing.Size(39, 13);
            this.lblParkingPlaces.TabIndex = 7;
            this.lblParkingPlaces.Text = "Places";
            this.lblParkingPlaces.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbxParkingPlaces);
            this.groupBox1.Controls.Add(this.lblParkingPlaces);
            this.groupBox1.Controls.Add(this.checkBoxParking);
            this.groupBox1.Location = new System.Drawing.Point(15, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 69);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parking";
            // 
            // checkBoxIsGroup
            // 
            this.checkBoxIsGroup.AutoSize = true;
            this.checkBoxIsGroup.Enabled = false;
            this.checkBoxIsGroup.Location = new System.Drawing.Point(15, 230);
            this.checkBoxIsGroup.Name = "checkBoxIsGroup";
            this.checkBoxIsGroup.Size = new System.Drawing.Size(66, 17);
            this.checkBoxIsGroup.TabIndex = 9;
            this.checkBoxIsGroup.Text = "Is Group";
            this.checkBoxIsGroup.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(81, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Discount 7%";
            // 
            // btnAddRoom
            // 
            this.btnAddRoom.Location = new System.Drawing.Point(253, 155);
            this.btnAddRoom.Name = "btnAddRoom";
            this.btnAddRoom.Size = new System.Drawing.Size(69, 23);
            this.btnAddRoom.TabIndex = 11;
            this.btnAddRoom.Text = "Add Room";
            this.btnAddRoom.UseVisualStyleBackColor = true;
            this.btnAddRoom.Click += new System.EventHandler(this.btnAddRoom_Click);
            // 
            // txtReservationId
            // 
            this.txtReservationId.Location = new System.Drawing.Point(305, 9);
            this.txtReservationId.Name = "txtReservationId";
            this.txtReservationId.Size = new System.Drawing.Size(20, 20);
            this.txtReservationId.TabIndex = 12;
            this.txtReservationId.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(253, 262);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 297);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtReservationId);
            this.Controls.Add(this.btnAddRoom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBoxIsGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridViewRooms);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpCheckOut);
            this.Controls.Add(this.dtpCheckIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Reservation";
            this.Text = "Reservation";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRooms)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpCheckIn;
        private System.Windows.Forms.DateTimePicker dtpCheckOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewRooms;
        private System.Windows.Forms.CheckBox checkBoxParking;
        private System.Windows.Forms.ComboBox cmbxParkingPlaces;
        private System.Windows.Forms.Label lblParkingPlaces;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxIsGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddRoom;
        private System.Windows.Forms.TextBox txtReservationId;
        private System.Windows.Forms.Button btnNext;
    }
}