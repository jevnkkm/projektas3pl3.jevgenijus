﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Services;
using Hotel.ViewModels;
using Hotel.Dal.Enumerations;
using Hotel.Main.Utils;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Main
{
    public partial class Reservation : BaseForm
    {
        private ReservationService _reservationService;
        private ReservationViewModel _reservation;
        public Reservation(ReservationViewModel viewModel = null)
        {
            InitializeComponent();
            CofigureGrid();
            _reservationService = new ReservationService();
            if (viewModel == null)
            {
                dtpCheckIn.Value = DateTime.Now;
                dtpCheckOut.Value = DateTime.Now.AddDays(2);
                _reservation = new ReservationViewModel();
                _reservation.Id = _reservationService.CreateReservation();
                txtReservationId.Text = _reservation.Id.ToString();
            } else
            {
                _reservation = viewModel;
                FillForm();
            }
            FillDataGridView();
        }

        private void FillForm()
        {
            txtReservationId.Text = _reservation.Id.ToString();
            checkBoxIsGroup.Checked = _reservation.IsGroup;
            //TODO: ParkingData NULL
            checkBoxParking.Checked = _reservation.ParkingData.IsNeedParking;
            dtpCheckIn.Value = _reservation.CheckIn;
            dtpCheckOut.Value = _reservation.CheckOut;
            if (_reservation.ParkingData.IsNeedParking)
            {
                cmbxParkingPlaces.SelectedItem = _reservation.ParkingData.ParkingSpaces.ToString();
            }

        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "Id",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridViewRooms.Columns.Add(idColumn);

            DataGridViewTextBoxColumn reservationColumn = new DataGridViewTextBoxColumn
            {
                Name = "ReservationId",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridViewRooms.Columns.Add(reservationColumn);

            DataGridViewTextBoxColumn roomTypeColumn = new DataGridViewTextBoxColumn
            {
                Name = "RoomType",
                HeaderText = "Room Type",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 120,
                Resizable = DataGridViewTriState.False
            };
            dataGridViewRooms.Columns.Add(roomTypeColumn);

            DataGridViewTextBoxColumn accommColumn = new DataGridViewTextBoxColumn
            {
                Name = "AccommodationType",
                HeaderText = "Accommodation Type",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 150,
                Resizable = DataGridViewTriState.False
            };
            dataGridViewRooms.Columns.Add(accommColumn);

            DataGridViewTextBoxColumn quantityColumn = new DataGridViewTextBoxColumn
            {
                Name = "Quantity",
                HeaderText = "quantityColumn",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 30,
                Resizable = DataGridViewTriState.False
            };
            dataGridViewRooms.Columns.Add(quantityColumn);

            DataGridViewTextBoxColumn tempRoomObject = new DataGridViewTextBoxColumn
            {
                Name = "TempRoomObject",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridViewRooms.Columns.Add(tempRoomObject);

            DataGridViewButtonColumn editButton = new DataGridViewButtonColumn
            {
                Name = "Edit",
                HeaderText = "Edit",
                UseColumnTextForButtonValue = true,
                Text = "Edit",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dataGridViewRooms.Columns.Add(editButton);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dataGridViewRooms.Columns.Add(deleteButton);

        }
        private void checkBoxParking_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                lblParkingPlaces.Show();
                cmbxParkingPlaces.Show();
            }
            else
            {
                lblParkingPlaces.Hide();
                cmbxParkingPlaces.Hide();
                cmbxParkingPlaces.SelectedIndex = -1;
            }
        }

        private void btnAddRoom_Click(object sender, EventArgs e)
        {
            var reservationId = Convert.ToInt32(txtReservationId.Text);
            RoomAdd roomAdd = new RoomAdd(reservationId);
            roomAdd.FormClosed += RoomAdd_FormClosed;
            roomAdd.ShowDialog();

        }

        private void RoomAdd_FormClosed(object sender, FormClosedEventArgs e)
        {
            FillDataGridView();
        }

        private void dataGridViewRooms_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void FillDataGridView()
        {
            var reservationId = int.Parse(txtReservationId.Text);
            var rooms = _reservationService.GetTempRooms(reservationId);

            dataGridViewRooms.Rows.Clear();

            _reservation.TempRooms = rooms;

            foreach (var room in rooms)
            {
                var roomType = (RoomTypeEnum)Enum.ToObject(typeof(RoomTypeEnum), room.RoomTypeId);
                var rowIndex = dataGridViewRooms.Rows.Add();
                dataGridViewRooms.Rows[rowIndex].Cells["Id"].Value = room.Id;
                dataGridViewRooms.Rows[rowIndex].Cells["ReservationId"].Value = room.ReservationId;
                dataGridViewRooms.Rows[rowIndex].Cells["RoomType"].Value = ((RoomTypeEnum)room.RoomTypeId).GetAttribute<DisplayAttribute>().Name;
                dataGridViewRooms.Rows[rowIndex].Cells["AccommodationType"].Value = ((AccommodationTypeEnum)room.AccommodationTypeId).GetAttribute<DisplayAttribute>().Name;
                dataGridViewRooms.Rows[rowIndex].Cells["Quantity"].Value = room.Quantity;
                dataGridViewRooms.Rows[rowIndex].Cells["TempRoomObject"].Value = room;
                dataGridViewRooms.Rows[rowIndex].Cells["Edit"].Value = "Edit";
                dataGridViewRooms.Rows[rowIndex].Cells["Delete"].Value = "Delete";
            }
            _reservation.MapLists();
        }

        private void dataGridViewRooms_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to delete this room?", "Delete room", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    var id = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["Id"].Value);
                    MessageBox.Show(_reservationService.DeleteTempRoom(id).ToString());
                    FillDataGridView();
                }
            }

            if (e.ColumnIndex == 6)
            {
                var viewModel = (TempRoomAddViewModel)((DataGridView)sender).Rows[e.RowIndex].Cells["TempRoomObject"].Value;

                var tempRoomSaveForm = new RoomAdd(viewModel: viewModel);
                tempRoomSaveForm.ShowDialog();
                FillDataGridView();
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            _reservation.CheckIn = dtpCheckIn.Value;
            _reservation.CheckOut = dtpCheckOut.Value;
            _reservation.ParkingData.IsNeedParking = checkBoxParking.Checked;
            if (_reservation.ParkingData.IsNeedParking)
            {
                _reservation.ParkingData.ParkingSpaces = int.Parse(cmbxParkingPlaces.SelectedItem.ToString());
            }
            var reservationFinilizeForm = new FinilizeReservation(_reservation);
            this.Close();
            reservationFinilizeForm.Show();
        }
    }
}
