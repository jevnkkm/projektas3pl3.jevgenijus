﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.Entities
{
    public class Room
    {
        public int? RoomId { get; set; }

        public int RoomNumber { get; set; }

        public int RoomTypeId { get; set; }

        public string AdditionalInfo { get; set; }

        public int AccommodationType { get; set; }

        public bool IsFree { get; set; }

        private Room()
        {
        }

        public Room(int roomNumber, int roomTypeId, string additionInfo, int accommodationType, bool isFree, int? roomId = null)
        {
            RoomNumber = roomNumber;
            RoomTypeId = roomTypeId;
            AdditionalInfo = additionInfo;
            AccommodationType = accommodationType;
            IsFree = isFree;
            RoomId = roomId;
        }
    }
}
